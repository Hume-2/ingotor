-- ######################################################
--           Ingotor grind lua tube 
--         by Guill4um on august, 21th of 2020
-- ######################################################

-- this lua tube check if the item can be sent to this machine line
-- the ping on the blue tube by a stack show that the machine is busy if in grind mode

-- ** settings **
local  LINE_NB = 1 -- 1 is the first line near the source of the items , then increase
local LCD_CHANNEL = "LCD"

-- ** functions **
-- display a message on the lcd
function display(message, ...)
    digiline_send(LCD_CHANNEL, message:format(...))
end


-- ** events **
if event.type == "item" then
    -- ping on the blue  tube by a stack
    if event.pin.name == "blue" then
        if mem.grinding then
            -- set the line as busy if not
            if not mem.is_busy then
                mem.is_busy = true
                digiline_send("processing",{step = "sending", action = "BUSY_LINE", line = LINE_NB})
            end
            -- check if the line is busy if not another ping after 10s (else the interrupt is thrown again replacing this one until no more ping)
            interrupt(10,"free_line")
        end
        return "blue" 
    end -- retrun item to  where they come from
    
    -- fast sent settings
    if  mem.sum_to_line == nil then  mem.sum_to_line = 0 end
    local max = mem.nb_of_lines * 2 -- number of stack to send fast to lines
    if  mem.to_line_count == nil then  mem.to_line_count = 0 end
    -- if we are in the firsts fast sending to busy all machines :
    if  mem.sum_to_line < max then
        -- count the stack sent to this line for firsts fast sending to busy all machine quick
        if  mem.to_line_count == nil then  mem.to_line_count = 0 end
        if (mem.to_line_count == 0 and mem.sum_to_line < mem.nb_of_lines) or (mem.to_line_count == 1 and mem.sum_to_line >=  mem.nb_of_lines)  then
            mem.to_line_count = mem.to_line_count + 1
            mem.sum_to_line = mem.sum_to_line + 1
            digiline_send("processing",{step = "sending", action = "ADD_SUM_TO_LINE", line = LINE_NB})
            if mem.grinding then
                -- check if the line is busy after 10s
                if mem.to_line_count >= 2 then interrupt(10,"free_line") end
                return "blue" -- sent to grind
            else 
                return "green"-- avoid grinding
            end
        else
            return "white" -- sent to the next line
        end
    elseif not mem.is_busy  then
        if mem.grinding then
             -- check if the line is busy after 10s
            if mem.to_line_count >= 2 then interrupt(10,"free_line") end
            return "blue" -- sent to grind
        else 
            return "green"-- avoid grinding
        end
    else
        return "white" -- sent to the next line
    end
elseif event.type == "digiline" then 
    -- setting on digiline message
    if event.channel == "settings" then 
        if (event.msg == "truegrinding") then
            mem.grinding = true
        elseif (event.msg == "falsegrinding") then
        -- reset and free memory
        elseif event.msg == "CLEAR" then
            mem.grinding = nil
            mem.is_busy = nil
            mem.to_line_count = nil
            mem.nb_of_lines = nil
            mem.sum_to_line = nil
        end
    elseif event.channel == "processing" then 
        -- busy and free the line  (for bake only mode)
        if event.msg.action == "BUSY_LINE" and event.msg.line == LINE_NB then
            mem.is_busy = true
        elseif event.msg.action == "FREE_LINE" and event.msg.line == LINE_NB then
            mem.is_busy = false
        -- update number sent to all lines (for the firsts fast sendings)
        elseif event.msg.action == "UPDATE_SUM_TO_LINE" then
            mem.sum_to_line = event.msg.sum_to_line
        end
    elseif event.channel == "settings_nb_lines" then 
        -- total of number of line of work machine
        mem.nb_of_lines = event.msg
    end
elseif event.type == "interrupt" then -- on an interrupt raise
    -- set the line as free
    if event.iid == "free_line" then
        mem.is_busy = false
        digiline_send("processing",{step = "sending", action = "FREE_LINE", line = LINE_NB})
    end
end