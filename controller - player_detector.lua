-- ######################################################
--               Ingotor detector controller 
--         by Guill4um on august, 21th of 2020
-- ######################################################

-- this controller allow to detect players and to  start a processing that
--  output in the locked chest (intead of chest for public use)

-- ** settings **
local LCD_CHANNEL = "LCD"
local function is_player_detect() return pin.b end --set the good pin here too in lowercase ex: pin.b
local function is_technician_detect() return pin.a end --set the good pin here too in lowercase ex: pin.b
local PIN_RESET = "D" -- set the good pin in UPPERCASE ex : "B"

local function display(message, ...)
    digiline_send(LCD_CHANNEL, message:format(...))
end

-- ** event **
if event.type == "digiline" then 
    if event.channel == "settings" then
        if event.msg == "PLAYER_DETECT" then
            digiline_send("settings",  { action = "SET_OUTPUT", is_locked_chest = is_player_detect() })
        end
    end
elseif event.type == "on" then 
    if event.pin.name == PIN_RESET then 
        if is_technician_detect() then
            digiline_send("settings","CLEAR")
        end
    end
end