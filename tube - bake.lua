-- ######################################################
--               Ingotor bake lua tube 
--         by Guill4um on august, 21th of 2020
-- ######################################################

-- this  lua tube is to choose if a stack is to be sent to the machine
-- the ping on the blue tube by a stack show that the machine is busy if in back only mode

-- ** settings **
local  LINE_NB = 1 -- 1 is the first chest near the source of the items , then increase
local LCD_CHANNEL = "LCD"

-- ** functions **
-- display a message on the lcd
function display(message, ...)
    digiline_send(LCD_CHANNEL, message:format(...))
end

-- ** events **
if event.type == "item" then
    -- ping on the blue  tube by a stack
    if event.pin.name == "blue" then 
        if not mem.grinding then -- if bake only mode
            -- set the line as busy if not
            if not mem.is_busy then
                mem.is_busy = true
                digiline_send("processing",{step = "sending", action = "BUSY_LINE", line = LINE_NB})
            end
            -- check if the line is busy if not another ping after 10s (else the interrupt is thrown again replacing this one until no more ping)
            interrupt(10,"free_line")
        end
        return "blue" 
    end -- retrun item to  where they come from
    
    if mem.baking then 
        --  step for compressing  item name in the list :
        -- -4, sapling => & , cobble => | nic: => 1: , aroa: => 2: , ralia: => 3: , trees: => 4:, 
        --rch: => 5:, ressed: => 6, ult: => 7:
        
        -- compress the current item name
        local zip_name = string.sub(event.item.name, 5)  
        zip_name = zip_name:gsub("sapling", "&")
        zip_name = zip_name:gsub("cobble", "|")
        zip_name = zip_name:gsub("nic:", "1:",1)
        zip_name = zip_name:gsub("aroa:", "2:",1)
        zip_name = zip_name:gsub("ralia:", "3:",1)
        zip_name = zip_name:gsub("trees:", "4:",1)
        zip_name = zip_name:gsub("rch:", "5:",1)
        zip_name = zip_name:gsub("ressed:", "6:",1)
        zip_name = zip_name:gsub("ult:", "7:",1)
        zip_name = zip_name:gsub("real:", "8:",1)
        zip_name = zip_name:gsub("tree", "~",1)
        zip_name = zip_name:gsub("trunk", "#",1)
        zip_name = zip_name:gsub("dust", "`",1)
        zip_name = zip_name:gsub("&_ongen", "&",1)

        

        -- if bake only mode :
        if not mem.grinding then 
            -- count the stack sent to this line for firsts fast sending to busy all machine quick
            if  mem.to_line_count == nil then  mem.to_line_count = 0 end
            mem.to_line_count = mem.to_line_count + 1
            -- check if the line is busy after 10s
            if mem.to_line_count >= 2 then interrupt(10,"free_line") end
        end

        -- check if the  item is grindable
        if not mem.bakeable_str:find(zip_name.." ") then 
            return "green" -- avoid baking
        end

        return "blue" -- sent to bake
    else 
        return "green" -- avoid baking
    end
elseif event.type == "interrupt" then -- on an interrupt raise
    -- set the line as free
    if event.iid == "free_line" then
        mem.is_busy = false
        digiline_send("processing",{step = "sending", action = "FREE_LINE", line = LINE_NB})
    end
elseif event.type == "digiline" then
    -- setting on digiline message
    if event.channel == "settings" then 
        -- machine mode
        if (event.msg == "truebaking") then
            mem.baking = true
        elseif (event.msg == "falsebaking") then
            mem.baking = false
        elseif (event.msg == "truegrinding") then
            mem.grinding = true
        elseif (event.msg == "falsegrinding") then
            mem.grinding = false
        -- reset and free memory
        elseif event.msg == "CLEAR" then
            mem.baking = nil
            mem.grinding = nil
            mem.is_busy = nil
            mem.to_line_count = nil
            mem.bakeable_str = nil
        --  when the machine start set the bakeable list
        elseif event.msg == "test_started" then
            -- manually compressed string of all bakeable item, (tell me on discord in mp if one is missing)
            -- keep it with a 0 indent space before each line
            mem.bakeable_str = [[
ents:silicon_dioxide 
ing:potato 
7:dirt 
7:gravel 

1:brass_` 
1:bronze_` 
1:carbon_steel_` 
1:cast_iron_` 
1:chernobylite_` 
1:chromium_` 
1:copper_` 
1:lead_` 
1:gold_` 
1:mithril_` 
1:silver_` 
1:stainless_steel_` 
1:stone_` 
1:tin_` 
1:wrought_iron_` 
1:zinc_` 
1:akalin_` 
1:alatro_` 
1:arol_` 
1:talinite_` 
1:°0_` 
1:°1_` 
1:°2_` 
1:°3_` 
1:°4_` 
1:°5_` 
1:°6_` 
1:°_` 
1:°8_` 
1:°9_` 
1:°10_` 
1:°11_` 
1:°12_` 
1:°13_` 
1:°14_` 
1:°15_` 
1:°16_` 
1:°17_` 
1:°18_` 
1:°19_` 
1:°20_` 
1:°21_` 
1:°22_` 
1:°23_` 
1:°24_` 
1:°25_` 
1:°26_` 
1:°27_` 
1:°28_` 
1:°29_` 
1:°30_` 
1:°31_` 
1:°32_` 
1:°33_` 
1:°34_` 
1:°35_` 
1:raw_latex 
c_materials:oil_extract 
c_materials:wet_cement 
c_materials:paraffin 
canoak:& 
2:black_beech_& 
2:black_maire_& 
2:cabbage_~_& 
2:common_~_daisy_& 
2:hinau_& 
2:kahikatea_& 
2:kamahi_& 
2:karaka_& 
2:karo_& 
2:kauri_& 
2:kawakawa_& 
2:kokomuka_& 
2:kowhai_& 
2:leatherwood_& 
2:mahoe_& 
2:mamaku_& 
2:mangrove_& 
2:manuka_& 
2:matagouri_& 
2:miro_& 
2:mountain_beech_& 
2:nikau_palm_& 
2:pahautea_& 
2:pohutukawa_& 
2:rangiora_& 
2:rimu_& 
2:silver_beech_& 
2:silver_fern_& 
2:tawa_& 
2:totara_& 
2:wheki_& 
3:black_box_& 
3:black_wattle_& 
3:blue_gum_& 
3:boab_& 
3:bull_banksia_& 
3:celery_top_pine_& 
3:cherry_& 
3:cloncurry_box_& 
3:coast_banksia_& 
3:coolabah_& 
3:dain~_stringybark_& 
3:darwin_woollybutt_& 
3:desert_oak_& 
3:fan_palm_& 
3:golden_wattle_& 
3:grey_mangrove_& 
3:huon_pine_& 
3:illawarra_flame_& 
3:jarrah_& 
3:karri_& 
3:lemon_eucalyptus_& 
3:lemon_myrtle_& 
3:lilly_pilly_& 
3:macadamia_& 
3:mangrove_apple_& 
3:marri_& 
3:merbau_& 
3:moreton_bay_fig_& 
3:mulga_& 
3:paperbark_& 
3:quandong_& 
3:red_bottlebrush_& 
3:river_oak_& 
3:river_red_gum_& 
3:rottnest_island_pine_& 
3:&_giant_~_fern 
3:&_~_fern 
3:scribbly_gum_& 
3:shoestring_acacia_& 
3:snow_gum_& 
3:southern_sassafras_& 
3:stilted_mangrove_& 
3:sugar_gum_& 
3:swamp_bloodwood_& 
3:swamp_gum_& 
3:swamp_paperbark_& 
3:tasmanian_myrtle_& 
3:tea_~_& 
3:white_box_& 
3:wirewood_& 
ab:& 
elabra:& 
rlebanon:& 
ry~:& 
tnut~:& 
entine~:& 
7:acacia_bush_& 
7:acacia_& 
7:aspen_& 
7:blueberry_bush_& 
7:bush_& 
7:emergent_jungle_& 
7:jungle& 
7:pine_bush_& 
7:pine_& 
7:& 
y:& 
8:banana_~_& 
8:big_~_& 
8:birch_& 
8:frost_~_& 
8:mushroom_& 
8:orange_~_& 
8:palm_& 
8:redwood_& 
8:sakura_& 
8:willow_& 
8:yellow_~_&
8:palmleaves 
r~:& 
o~:& 
randa:& 
h:& 
n~:& 
la:& 
4:apple_~_& 
4:beech_& 
4:birch_& 
4:cedar_& 
4:date_palm_& 
4:fir_& 
4:jungle~_& 
4:oak_& 
4:palm_& 
4:poplar_& 
4:poplar_small_& 
4:rubber_~_& 
4:sequoia_& 
4:spruce_& 
4:willow_& 
l:giant_palm_& 
l:natal_cycad_& 
l:prince_cycad_& 
l:~_fern_& 
l:woodcycad_& 
apple:& 
morefig:& 
ellathorn:& 
cons_materials:glue 
7:mese_crystal_fragment 
7:mese_crystal 
7:mese 
5:blackeye_item 
5:greeneye_item 
5:redeye_item 
5:purpleeye_item 
6:afualite 
6:amphibolite 
6:andesite 
6:aplite 
6:basalt 
6:dark_vindesite 
6:diorite 
6:dolomite 
6:emutite 
6:gabbro 
6:gneiss 
6:granite 
6:green_slimestone 
6:hektorite 
6:limestone 
6:marble 
6:omphyrite 
6:pegmatite 
6:peridotite 
6:phonolite 
6:phylite 
6:purple_slimestone 
6:quartzite 
6:red_slimestone 
6:schist 
6:sichamine 
6:slate 
6:vindesite 
5:mossy_gravel 
5:clay_lump 
5:green_slimy_block 
5:purple_slimy_block 
5:red_slimy_block 
5:black_slimy_block 
5:afualite_| 
5:amphibolite_| 
5:andesite_| 
5:aplite_| 
5:basalt_| 
5:dark_vindesite_| 
5:diorite_| 
5:dolomite_| 
5:emutite_| 
5:gabbro_| 
5:gneiss_| 
5:granite_| 
5:green_slimestone_| 
5:hektorite_| 
5:limestone_| 
5:marble_| 
5:omphyrite_| 
5:pegmatite_| 
5:peridotite_| 
5:phonolite_| 
5:phylite_| 
5:purple_slimestone_| 
5:quartzite_| 
5:red_slimestone_| 
5:schist_| 
5:sichamine_| 
5:slate_| 
5:vindesite_| 
5:amphibolite_mossy_| 
5:andesite_mossy_| 
5:aplite_mossy_| 
5:basalt_mossy_| 
5:dark_vindesite_mossy_| 
5:diorite_mossy_| 
5:dolomite_mossy_| 
5:gabbro_mossy_| 
5:gneiss_mossy_| 
5:granite_mossy_| 
5:limestone_mossy_| 
5:marble_mossy_| 
5:pegmatite_mossy_| 
5:peridotite_mossy_| 
5:phonolite_mossy_| 
5:phylite_mossy_| 
5:quartzite_mossy_| 
5:schist_mossy_| 
5:sichamine_mossy_| 
5:slate_mossy_| 
5:vindesite_mossy_| 
2:iron_sand_with_pipi 
2:river_sand 
2:sand_with_pipi 
2:volcanic_sand 
3:red_sand 
7:desert_sand 
7:sand 
7:silver_sand 
8:quicksand 
8:quicksand2 
7:obsidian_shard 
7:| 
7:mossy| 
7:desert_| 
7:clay_lump 
7:copper_lump 
7:gold_lump 
7:key 
7:skeleton_key 
7:iron_lump 
7:tin_lump 
ing:flour 
els:glass_fragments 
els:steel_bottle
7:clay 
2:hinau_~ 
2:kamahi_~ 
2:rangiora_~ 
2:bracken 
2:raupo 
2:raupo_flower 
2:seed_meal 
2:mamaku_crown 
2:mamaku_~ 
2:cabbage_~_~ 
2:cabbage_~_crown 
2:nikau_palm_skirt 
2:crushed_lime 
2:volcanic_sand 
2:schist 
2:granite 
2:siltstone 
2:claystone 
2:pale_sandstone 
2:grey_sandstone 
7:sandstone 
7:silver_sandstone 
7:desert_sandstone 
2:coquina_limestone 
7:coral 
2:greywacke 
2:andesite 
2:scoria 
2:basalt 

canoak:# 
2:black_beech_~ 
2:black_maire_~ 
2:cabbage_~_~ 
2:common_~_daisy_~ 
2:hinau_~ 
2:kahikatea_~ 
2:kamahi_~ 
2:karaka_~ 
2:karo_~ 
2:kauri_~ 
2:kawakawa_~ 
2:kowhai_~ 
2:mahoe_~ 
2:mamaku_~ 
2:mangrove_~ 
2:manuka_~ 
2:matagouri_~ 
2:miro_~ 
2:mountain_beech_~ 
2:nikau_palm_skirt 
2:nikau_palm_~ 
2:pahautea_~ 
2:pohutukawa_~ 
2:rangiora_~ 
2:rimu_~ 
2:silver_beech_~ 
2:silver_fern_~ 
2:tawa_~ 
2:totara_~ 
iTop 
2:wheki_skirt 
2:wheki_~ 
3:black_box_~ 
3:black_wattle_~ 
3:blue_gum_~ 
3:boab_~ 
3:bull_banksia_~ 
3:celery_top_pine_~ 
3:cherry_~ 
3:cloncurry_box_~ 
3:coast_banksia_~ 
3:coolabah_~ 
3:dain~_stringybark_~ 
3:darwin_woollybutt_~ 
3:desert_oak_~ 
3:fan_palm_~ 
3:fern_# 
3:fern_#_big 
3:golden_wattle_~ 
3:grey_mangrove_~ 
3:huon_pine_~ 
3:illawarra_flame_~ 
3:jarrah_~ 
3:karri_~ 
3:lemon_eucalyptus_~ 
3:lemon_myrtle_~ 
3:lilly_pilly_~ 
3:macadamia_~ 
3:mangrove_apple_~ 
3:marri_~ 
3:merbau_~ 
3:moreton_bay_fig_~ 
3:mulga_~ 
3:paperbark_~ 
3:quandong_~ 
3:red_bottlebrush_~ 
3:river_oak_~ 
3:river_red_gum_~ 
3:rottnest_island_pine_~ 
3:scribbly_gum_~ 
3:shoestring_acacia_~ 
3:snow_gum_~ 
3:southern_sassafras_~ 
3:stilted_mangrove_~ 
3:sugar_gum_~ 
3:swamp_bloodwood_~ 
3:swamp_gum_~ 
3:swamp_paperbark_~ 
3:tasmanian_myrtle_~ 
3:tea_~_~ 
3:white_box_~ 
3:wirewood_~ 
ab:# 
elabra:# 
rlebanon:# 
ry~:# 
tnut~:# 
entine~:# 
ages:barrel 
ages:tub 
ages:water_gen 
7:acacia_~ 
7:aspen_~ 
7:jungle~ 
7:pine_~ 
7:~ 
y:# 
8:banana_# 
8:birch_# 
8:frost_~ 
8:mushroom_# 
8:palm_# 
8:redwood_# 
8:sakura_# 
8:scorched_~ 
8:willow_# 
8:yellow_# 
r~:# 
o~:# 
randa:# 
h:mushroom_# 
n~:# 
la:# 
blocks:all_faces_acacia_~ 
blocks:all_faces_aspen_~ 
blocks:all_faces_jungle_~ 
blocks:all_faces_mushroom_~ 
blocks:all_faces_pine_~ 
blocks:all_faces_~ 
4:cedar_# 
4:date_palm_ffruit_# 
4:date_palm_fruit_# 
4:date_palm_mfruit_# 
4:date_palm_# 
4:fir_# 
4:oak_# 
4:poplar_# 
4:rubber_~_# 
4:rubber_~_#_empty 
4:sequoia_# 
4:spruce_# 
4:willow_# 
l:giant_palm_# 
l:natal_cycad_# 
l:prince_cycad_# 
l:~_fern_# 
l:woodcycad_# 
morefig:# 
ellathorn:# 
ing:corn 
1:cornmeal 
            ]]
        end
    end
end