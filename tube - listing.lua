-- ######################################################
--           Ingotor listing lua tube 
--         by Guill4um on august, 21th of 2020
-- ######################################################

-- this lua tube check if the item can be sent to grind machines
-- and it list all item stack to process, and send them to machine when machine lines are free
    
    -- ** settings **
    local OUTPUT = "black"
    local RETURN = "green"
    local LCD_CHANNEL = "LCD"
    local NB_OF_LINES = 10 -- number of machines line
    local MINI_DELAY = 1.5 --  minimal secondes between two send
    
    -- ** functions **
    -- display message on the lcd
    local function display(message, ...)
        digiline_send(LCD_CHANNEL, message:format(...))
    end
    
    -- -- function that can be used in this controller or on digiline or on interrupt
    function processing(arg)
        if arg.step == "sending" then
            -- send the next item to the machines
            if arg.action == "SEND_NEXT" then
                local item = nil
                local left_item = nil

                if mem.list[1] ~= nil then
                    
                digiline_send("debug",(string.format("=>>item:%s left:%s",mem.list[1].item.count,mem.list[1].left)))
                    if mem.list[1].item.count > 0 then
                        item = mem.list[1].item
                    end
                    if mem.list[1].left ~= nil and mem.list[1].left > 0 then
                        left_item = {name = mem.list[1].item.name, count = mem.list[1].left}
                    end
                end

                if item ~= nil then
                    if mem.nb_busy_lines == nil then  mem.nb_busy_lines = 0 end
                    if mem.nb_busy_lines < NB_OF_LINES then
                        
                        if mem.last_send == nil then  mem.last_send = 0 end
                        local diff = os.clock() - mem.last_send
                        if diff >= MINI_DELAY then
                            digiline_send("injector_send", item)
                            mem.last_send = os.clock() 
                            table.remove(mem.list, 1)
                        else
                            interrupt(MINI_DELAY *1.1 - diff ,"send_fast_first")
                        end
                    end
                end
                if left_item ~= nil then
                    digiline_send("injector_left", left_item)
                end
            -- update for lua tube in lines the number of item sent to all lines 
            elseif arg.action == "ADD_SUM_TO_LINE" then
                
                if  mem.sum_to_line == nil then  mem.sum_to_line = 0 end
                mem.sum_to_line = mem.sum_to_line + 1
                digiline_send("processing",{step = "sending", action = "UPDATE_SUM_TO_LINE", sum_to_line = mem.sum_to_line})
                
            -- one line just become free , then update the number of busy line and send another stack
            elseif arg.action == "FREE_LINE" then
                mem.nb_busy_lines = mem.nb_busy_lines - 1
                processing({step = "sending", action = "SEND_NEXT"})
            -- one line jsut say it's busy, update the number of busy line
            elseif arg.action == "BUSY_LINE" then
                mem.nb_busy_lines = mem.nb_busy_lines + 1
            end
            -- when process finished free memory and reset machine
        elseif arg.step == "is_finished" then
            if mem.nb_busy_lines == nil then  mem.nb_busy_lines = 0 end
            if mem.nb_busy_lines <= 0 then -- si tout est inoccupé
                display("Finished")
                mem.list = nil
                mem.nb_busy_lines = nil
                mem.sent_count = nil
                mem.sum_to_line =nil
                mem.grinding = nil
                mem.is_busy = nil
                mem.sum_to_line =nil
                mem.last_send = nil
                mem.flag_started = nil
                mem.grindable_str = nil
                digiline_send("settings","CLEAR")
            end
        end
    end
    
    -- ** events **
    if event.type == "item"  then
        local new_item = nil
        if( mem.grinding) then
            -- when grinding is activate, filter grindable items
            if not mem.grindable_str:find(event.item.name.." ") then 
                -- if item is not grindable, take another stack
                digiline_send("processing",{step = "listing", action = "TAKE_NEXT"})
                return RETURN -- directly sent to output
            end

            -- check if multiple is ok
            if (event.item.name == "xpanes:pane_flat") then
                local left = event.item.count % 8
                local multi = math.floor(event.item.count/8) * 8
                if ( multi > 0) then
                    mem.flag_started = "yes"
                end
                if mem.list == nil then  mem.list = {} end
                new_item = true
                table.insert(mem.list,{ item = { name = event.item.name , count = multi }, left = left })
                table.sort(mem.list, function(a,b) return a.item.count>b.item.count end) -- decrease sorting to take bigger stack first to avoid errors
            end
            
        end
        -- an item will be sent to the machines (to avoid reset of the machine)
        
        -- the stack is added to the list
        

        if (new_item == nil) then -- if not  grind item with multiple
            mem.flag_started = "yes"
            if mem.list == nil then  mem.list = {} end
            table.insert(mem.list,{ item = event.item, left = nil})
            table.sort(mem.list, function(a,b) return a.item.count>b.item.count end) -- decrease sorting to take bigger stack first to avoid errors  
        end
        
        -- first fast sendings
        if mem.sent_count == nil then mem.sent_count = 0 end
        if mem.sent_count < NB_OF_LINES * 2 then
            mem.sent_count = mem.sent_count + 1
            interrupt(MINI_DELAY * 1.1 * mem.sent_count ,"send_fast_first"..tostring(mem.sent_count))
        end
        
        -- take the next stack
        digiline_send("processing",{step = "listing", action = "TAKE_NEXT"})
        return OUTPUT -- sent to machines
    elseif event.type == "interrupt" then
        -- delayed firsts fast sent
        if  string.sub(event.iid, 1, 15)  ==  "send_fast_first" then
            processing({step = "sending", action = "SEND_NEXT"})
        -- test if the machine is started and an item is sent to machine , else reset the machine
        elseif event.iid == "test_started" then
            if mem.flag_started == "no" then
                processing({step = "is_finished"})
            end
        end
    elseif event.type == "digiline" then -- on a digiline message is recieved
        if event.channel == "processing" then 
            processing(event.msg) -- see this function 
        elseif event.channel == "settings" then 
            -- machine settings
            if (event.msg == "truegrinding") then
                digiline_send("settings_nb_lines",NB_OF_LINES)
                mem.grinding = true
            elseif (event.msg == "falsegrinding") then
                digiline_send("settings_nb_lines",NB_OF_LINES)
                mem.grinding = false
            elseif (event.msg == "truebaking") then
                mem.baking = true
            elseif (event.msg == "falsebaking") then
                mem.baking = false
            -- free memory
            elseif event.msg == "CLEAR" then
                display("Reset")
                mem.list = nil
                mem.nb_busy_lines = nil
                mem.sent_count = nil
                mem.grinding = nil
                mem.baking = nil
                mem.is_busy = nil
                mem.sum_to_line =nil
                mem.last_send = nil
                mem.flag_started = nil
                mem.grindable_str = nil
            -- at the begining test if machine is started with items and set the grindable string
            elseif event.msg == "test_started" then
                mem.flag_started = "no"
                interrupt(5,"test_started")
                -- list of all grindable item in the world, (tell me on discord in mp if one is missing)
                -- keep it with a 0 indent space before each line
                mem.grindable_str = [[
default:coal_lump 
default:copper_lump 
default:desert_stone 
default:gold_lump 
default:iron_lump 
default:tin_lump 
technic:chromium_lump 
technic:uranium_lump 
technic:zinc_lump 
technic:lead_lump 
technic:sulfur_lump 
basic_materials:brass_ingot 
default:bronze_ingot 
technic:carbon_steel_ingot 
technic:cast_iron_ingot 
technic:chernobylite_block 
technic:chromium_ingot 
default:copper_ingot 
technic:lead_ingot 
default:gold_ingot 
moreores:mithril_ingot 
moreores:silver_ingot 
technic:stainless_steel_ingot 
default:stone 
default:tin_ingot 
technic:wrought_iron_ingot 
technic:zinc_ingot 
glooptest:akalin_ingot 
glooptest:alatro_ingot 
glooptest:arol_ingot 
glooptest:talinite_ingot 
default:stone 
default:sand 
default:cobble 
default:gravel 
default:sandstone 
default:desert_sandstone 
default:silver_sandstone 
default:ice 
farming:seed_wheat 
moreores:mithril_lump 
moreores:silver_lump 
gloopores:alatro_lump 
gloopores:kalite_lump 
gloopores:arol_lump 
gloopores:talinite_lump 
gloopores:akalin_lump 
home_decor:brass_ingot 
technic:uranium0_ingot 
technic:uranium1_ingot 
technic:uranium2_ingot 
technic:uranium3_ingot 
technic:uranium4_ingot 
technic:uranium5_ingot 
technic:uranium6_ingot 
technic:uranium_ingot 
technic:uranium8_ingot 
technic:uranium9_ingot 
technic:uranium10_ingot 
technic:uranium11_ingot 
technic:uranium12_ingot 
technic:uranium13_ingot 
technic:uranium14_ingot 
technic:uranium15_ingot 
technic:uranium16_ingot 
technic:uranium17_ingot 
technic:uranium18_ingot 
technic:uranium19_ingot 
technic:uranium20_ingot 
technic:uranium21_ingot 
technic:uranium22_ingot 
technic:uranium23_ingot 
technic:uranium24_ingot 
technic:uranium25_ingot 
technic:uranium26_ingot 
technic:uranium27_ingot 
technic:uranium28_ingot 
technic:uranium29_ingot 
technic:uranium30_ingot 
technic:uranium31_ingot 
technic:uranium32_ingot 
technic:uranium33_ingot 
technic:uranium34_ingot 
technic:uranium35_ingot 
default:acacia_wood 
default:acacia_tree 
technic:acacia_grindings 
default:aspen_wood 
default:aspen_tree 
technic:aspen_grindings 
default:jungletree 
default:junglewood 
technic:jungletree_grindings 
default:pine_wood 
default:pine_tree 
technic:pine_grindings 
moretrees:rubber_tree_trunk_empty 
moretrees:rubber_tree_trunk 
moretrees:rubber_tree_planks 
technic:rubber_tree_grindings 
default:wood 
default:tree 
technic:tree_grindings 
underch:ruby 
underch:underground_vine 
underch:afualite 
underch:amphibolite 
underch:andesite 
underch:aplite 
underch:basalt 
underch:dark_vindesite 
underch:diorite 
underch:dolomite 
underch:emutite 
underch:gabbro 
underch:gneiss 
underch:granite 
underch:green_slimestone 
underch:hektorite 
underch:limestone 
underch:marble 
underch:omphyrite 
underch:pegmatite 
underch:peridotite 
underch:phonolite 
underch:phylite 
underch:purple_slimestone 
underch:quartzite 
underch:red_slimestone 
underch:schist 
underch:sichamine 
underch:slate 
underch:vindesite 
underch:afualite_cobble 
underch:amphibolite_cobble 
underch:andesite_cobble 
underch:aplite_cobble 
underch:basalt_cobble 
underch:dark_vindesite_cobble 
underch:diorite_cobble 
underch:dolomite_cobble 
underch:emutite_cobble 
underch:gabbro_cobble 
underch:gneiss_cobble 
underch:granite_cobble 
underch:green_slimestone_cobble 
underch:hektorite_cobble 
underch:limestone_cobble 
underch:marble_cobble 
underch:omphyrite_cobble 
underch:pegmatite_cobble 
underch:peridotite_cobble 
underch:phonolite_cobble 
underch:phylite_cobble 
underch:purple_slimestone_cobble 
underch:quartzite_cobble 
underch:red_slimestone_cobble 
underch:schist_cobble 
underch:sichamine_cobble 
underch:slate_cobble 
underch:vindesite_cobble 
compressed:afualite 
compressed:amphibolite 
compressed:andesite 
compressed:aplite 
compressed:basalt 
compressed:dark_vindesite 
compressed:diorite 
compressed:dolomite 
compressed:emutite 
compressed:gabbro 
compressed:gneiss 
compressed:granite 
compressed:green_slimestone 
compressed:hektorite 
compressed:limestone 
compressed:marble 
compressed:omphyrite 
compressed:pegmatite 
compressed:peridotite 
compressed:phonolite 
compressed:phylite 
compressed:purple_slimestone 
compressed:quartzite 
compressed:red_slimestone 
compressed:schist 
compressed:sichamine 
compressed:slate 
compressed:vindesite 
xpanes:pane_flat 
doors:door_wood 
farming:corn 
                                ]]
            end
        end
    end
    
    